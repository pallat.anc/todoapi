package todo

import (
	"testing"
)

type fakeStore struct{}

func (fakeStore) AutoMigrate(*Todo) {}
func (fakeStore) Save(*Todo) error  { return nil }

type mockContext struct {
	result interface{}
}

func (mockContext) Bind(i interface{}) error {
	*i.(*Todo) = Todo{
		Task: "nothing",
	}
	return nil
}
func (c *mockContext) JSON(code int, i interface{}) {
	c.result = i
}
func (mockContext) Audience() string { return "testing" }
func (mockContext) Log(string)       {}

func TestNotAcceptNothingTask(t *testing.T) {
	handler := &Handler{store: fakeStore{}}

	c := &mockContext{}

	handler.CreateTodoHandler(c)

	want := "not accept nothing task"
	get := c.result.(map[string]interface{})["error"]
	if want != get {
		t.Errorf("%q is wanted but got %q\n", want, get)
	}
}
