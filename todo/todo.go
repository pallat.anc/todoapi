package todo

import (
	"net/http"
	"time"
)

type Todo struct {
	ID        uint `gorm:"primarykey"`
	CreatedAt time.Time
	UpdatedAt time.Time
	Task      string `json:"text" binding:"required"`
}

type Handler struct {
	store Storer
}

func NewHandler(store Storer) *Handler {
	return &Handler{store: store}
}

type Storer interface {
	AutoMigrate(*Todo)
	Save(*Todo) error
}

type Context interface {
	Bind(interface{}) error
	JSON(int, interface{})
	Audience() string
	Log(string)
}

func (h *Handler) CreateTodoHandler(c Context) {
	h.store.AutoMigrate(&Todo{})

	var todo Todo
	if err := c.Bind(&todo); err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"error": err.Error(),
		})
		return
	}

	if todo.Task == "nothing" {
		aud := c.Audience()
		c.Log(aud + " do nothing")
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"error": "not accept nothing task",
		})
		return
	}

	err := h.store.Save(&todo)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"error": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, Success{
		ID: todo.ID,
	})
}

type Success struct {
	ID uint `json:"id" example:"1"`
}
