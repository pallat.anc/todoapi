package logger

import (
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

func LoggerMiddleware(logger *zap.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Set("logger", logger.With(zap.String("TransactionID", c.Request.Header.Get("TransactionID"))))
	}
}

func Log(c *gin.Context, s string) {
	logger, ok := c.Get("logger")
	if !ok {
		return
	}
	log, ok := logger.(*zap.Logger)
	if !ok {
		return
	}

	log.Info(s)
}
