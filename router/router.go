package router

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/cjexpress/tildi/pallat.anc/todoapi/auth"
	"gitlab.com/cjexpress/tildi/pallat.anc/todoapi/logger"
	"go.uber.org/zap"
)

type GinRouter struct {
	*gin.RouterGroup
	Engine *gin.Engine
}

func NewGinRouter() *GinRouter {
	r := gin.Default()
	logs := zap.NewExample()
	r.Use(logger.LoggerMiddleware(logs))
	config := cors.DefaultConfig()
	config.AllowOrigins = []string{
		"http://localhost:8080",
	}
	config.AllowHeaders = []string{
		"Origin",
		"Authorization",
		"TransactionID",
	}
	r.Use(cors.New(config))

	r.GET("/tokenz", auth.Login)
	v1 := r.Group("")
	// v1.Use(auth.Authen)

	return &GinRouter{RouterGroup: v1, Engine: r}
}

func (r *GinRouter) POST(path string, handler HandlerFunc) {
	r.RouterGroup.POST(path, NewGinHandlerFunc(handler))
}

type EchoRouter struct {
	*echo.Echo
}

func NewEchoRouter() *EchoRouter {
	r := echo.New()
	r.Use(middleware.Logger())
	return &EchoRouter{r}
}

func (r *EchoRouter) POST(path string, handler HandlerFunc) {
	r.Echo.POST(path, NewEchoHandlerFunc(handler))
}
