package router

import (
	"github.com/labstack/echo/v4"
)

func NewEchoHandlerFunc(handler HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		handler(NewEchoContext(c))
		return nil
	}
}

type EchoContext struct {
	echo.Context
}

func NewEchoContext(c echo.Context) *EchoContext {
	return &EchoContext{c}
}

func (c *EchoContext) Bind(i interface{}) error {
	return c.Context.Bind(i)
}

func (c *EchoContext) JSON(code int, i interface{}) {
	c.Context.JSON(code, i)
}

func (c *EchoContext) Audience() string {
	i := c.Get("Audience")
	if aud, ok := i.(string); ok {
		return aud
	}
	return ""
}

func (c *EchoContext) Log(s string) {
	// logger.Log()
}
