package router

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/cjexpress/tildi/pallat.anc/todoapi/logger"
	"gitlab.com/cjexpress/tildi/pallat.anc/todoapi/todo"
)

type HandlerFunc func(todo.Context)

func NewGinHandlerFunc(handler HandlerFunc) gin.HandlerFunc {
	return func(c *gin.Context) {
		handler(&Context{c})
	}
}

type Context struct {
	*gin.Context
}

func (c *Context) Bind(i interface{}) error {
	return c.ShouldBindJSON(i)
}

func (c *Context) JSON(code int, i interface{}) {
	c.Context.JSON(code, i)
}

func (c *Context) Audience() string {
	if aud, ok := c.Get("Audience"); ok {
		if s, ok := aud.(string); ok {
			return s
		}
	}

	return ""
}

func (c *Context) Log(s string) {
	logger.Log(c.Context, s)
}
