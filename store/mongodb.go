package store

import (
	"context"

	"gitlab.com/cjexpress/tildi/pallat.anc/todoapi/todo"
	"go.mongodb.org/mongo-driver/mongo"
)

type MongoDB struct {
	*mongo.Collection
}

func NewMongoDB(col *mongo.Collection) *MongoDB {
	return &MongoDB{Collection: col}
}

func (db *MongoDB) AutoMigrate(*todo.Todo) {

}

func (db *MongoDB) Save(t *todo.Todo) error {
	_, err := db.Collection.InsertOne(context.Background(), t)
	return err
}
