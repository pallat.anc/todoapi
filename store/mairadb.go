package store

import (
	"gitlab.com/cjexpress/tildi/pallat.anc/todoapi/todo"
	"gorm.io/gorm"
)

type MariaDB struct {
	db *gorm.DB
}

func NewMariaDB(db *gorm.DB) *MariaDB {
	return &MariaDB{db: db}
}

func (db *MariaDB) AutoMigrate(t *todo.Todo) {
	db.db.AutoMigrate(t)
}

func (db *MariaDB) Save(t *todo.Todo) error {
	return db.db.Create(t).Error
}
