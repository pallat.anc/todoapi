package auth

import (
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

type Credential struct {
	User string `json:"user" binding:"required"`
}

func Login(c *gin.Context) {
	mySigningKey := []byte("AllYourBase")

	// var cred Credential
	// if err := c.ShouldBindJSON(&cred); err != nil {
	// 	c.JSON(http.StatusUnauthorized, gin.H{
	// 		"error": "please contact admin",
	// 	})
	// 	return
	// }

	claims := &jwt.StandardClaims{
		Audience:  "test",
		ExpiresAt: time.Now().UTC().Add(5 * time.Minute).Unix(),
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	ss, err := token.SignedString(mySigningKey)
	if err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{
			"error": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"token": ss,
	})
}

func Authen(c *gin.Context) {
	mySigningKey := []byte("AllYourBase")
	authorization := c.Request.Header.Get("Authorization")
	tokenString := strings.TrimPrefix(authorization, "Bearer ")

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return mySigningKey, nil
	})

	if err != nil {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok {
		c.Set("Audience", claims["aud"])
	}

	c.Next()
}
