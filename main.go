package main

import (
	"context"
	"errors"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv" // gin-swagger middleware
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"golang.org/x/time/rate"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"

	// swagger embed files

	"gitlab.com/cjexpress/tildi/pallat.anc/todoapi/router"
	"gitlab.com/cjexpress/tildi/pallat.anc/todoapi/store"
	"gitlab.com/cjexpress/tildi/pallat.anc/todoapi/todo"
)

var (
	buildcommit = "-"
	buildtime   = "---"
)

func main() {
	_, err := os.Create("/tmp/live")
	if err != nil {
		log.Fatal(err)
	}
	defer os.Remove("/tmp/live")

	err = godotenv.Load()
	if err != nil {
		log.Println("use only os environment")
	}

	// logs := zap.NewExample()

	// r := gin.Default()
	// r.Use(logger.LoggerMiddleware(logs))

	// r.GET("/x", func(c *gin.Context) {
	// 	c.JSON(http.StatusOK, gin.H{
	// 		"buildcommit": buildcommit,
	// 		"buildtime":   buildtime,
	// 	})
	// })
	// r.GET("/healthz", func(c *gin.Context) {
	// 	c.JSON(http.StatusOK, gin.H{})
	// })
	// r.GET("/limits", LimitHandler)

	// r.POST("/login", auth.Login)

	// if os.Getenv("environment") == "dev" {
	// 	docs.SwaggerInfo.BasePath = "/"
	// 	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerfiles.Handler))
	// }

	// db := OpenDB()
	// s := store.NewMariaDB(db)
	col := OpenMongoDB()
	s := store.NewMongoDB(col)
	handler := todo.NewHandler(s)

	// v1 := r.Group("")
	// v1.Use(auth.Authen)

	r := router.NewGinRouter()
	r.POST("/todos", handler.CreateTodoHandler)

	srv := &http.Server{
		Addr:    ":" + os.Getenv("PORT"),
		Handler: r.Engine,
	}
	go func() {
		if err := srv.ListenAndServe(); err != nil && errors.Is(err, http.ErrServerClosed) {
			log.Printf("listen: %s\n", err)
		}
	}()

	quit := make(chan os.Signal)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Println("Shutting down server...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server forced to shutdown:", err)
	}

	log.Println("Server exiting")
}

func OpenDB() *gorm.DB {
	dsn := "root:my-secret-pw@tcp(localhost:3306)/myapp?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Panic(err)
	}
	return db
}

func OpenMongoDB() *mongo.Collection {
	client, err := mongo.Connect(context.Background(), options.Client().ApplyURI("mongodb://mongoadmin:secret@localhost:27017"))
	if err != nil {
		panic("failed to connect database")
	}
	return client.Database("myapp").Collection("todos")
}

var limiter = rate.NewLimiter(5, 5)

func LimitHandler(c *gin.Context) {
	if !limiter.Allow() {
		c.Status(http.StatusTooManyRequests)
		return
	}

	c.Status(http.StatusOK)
}
